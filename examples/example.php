<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\Criteria\EqualsCriteria;
use App\Criteria\LikeCriteria;
use App\Service\ReaderService;
use App\Criteria\AndCriteria;
use App\Services\Parsers\ArticleParser;

// Создаем критерию сравнения
$equalsFilter = new EqualsCriteria('status', 3);
// Создаем критерию поиска подстроки
$likeFilter = new LikeCriteria('name', 's');
// Создаем критерию "логичское И"
$andFilter = new AndCriteria($equalsFilter, $likeFilter);

// Путь до файла.
$csvFile = __DIR__ . '/data.csv';
// Создаем экземпляр парсера.
$parser = new ArticleParser();
// Создаем класс для чтения csv файла и читаем. Метод read() возвращает
// экземпляр класса Collection.
$input = (new ReaderService($csvFile, $parser))->read();
// Фильтруем загруженную коллекцию с помощью критерии.
$result = $input->filter($andFilter);

print_r($result->all());