<?php

use App\TypeHinting\BaseType;
use App\TypeHinting\StringType;
use App\TypeHinting\IntegerType;
use App\TypeHinting\DateTimeType;
use App\Exceptions\UnsupportedTypeException;

class BaseTypeTest extends PHPUnit_Framework_TestCase
{
    public function testCreationOfInteger()
    {
        $type = BaseType::getInstance(BaseType::INT);
        self::assertInstanceOf(IntegerType::class, $type);
    }

    public function testCreationOfString()
    {
        $type = BaseType::getInstance(BaseType::STRING);
        self::assertInstanceOf(StringType::class, $type);
    }

    public function testCreationOfDateTime()
    {
        $type = BaseType::getInstance(BaseType::DATETIME);
        self::assertInstanceOf(DateTimeType::class, $type);
    }

    public function testInstanceOfShouldThrowException()
    {
        $this->expectException(UnsupportedTypeException::class);
        BaseType::getInstance('WRONG_TYPE');
    }
}
