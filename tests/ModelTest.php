<?php

use App\Model;
use App\Exceptions\UnknownPropertyException;

class FakeModel2 extends Model
{
    protected $foo;
    protected $bar;

    /**
     * @param mixed $foo
     */
    public function setFoo($foo)
    {
        $this->foo = $foo;
    }

    /**
     * @return mixed
     */
    public function getFoo()
    {
        return $this->foo;
    }
}

class ModelTest extends PHPUnit_Framework_TestCase
{
    public function testMagicGetter()
    {
        $fake = new FakeModel2();
        $fake->setFoo('baz');
        self::assertEquals('baz', $fake->foo);
    }

    public function testFieldWithoutGetterShouldThrowException()
    {
        $fake = new FakeModel2();
        $this->expectException(UnknownPropertyException::class);
        $fake->bar;
    }
}
