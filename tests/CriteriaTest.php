<?php

use App\Collection;
use App\Criteria\EqualsCriteria;
use App\Models\Article;
use App\Criteria\AndCriteria;
use App\Criteria\OrCriteria;
use App\Criteria\LikeCriteria;
use App\Exceptions\CriteriaException;

class CriteriaTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Collection
     */
    private $collection;

    public function setUp()
    {
        $this->collection = $collection = Collection::make([
            new Article('first', new DateTime('08.11.2016'), 1),
            new Article('second', new DateTime('08.11.2016'), 2),
            new Article('third', new DateTime('09.11.2016'), 3),
            new Article('fourth', new DateTime('10.11.2016'), 4),
        ]);
    }

    public function testEqualsForInteger()
    {
        $criteria = new EqualsCriteria('status', 1);
        $result = $this->collection->filter($criteria);
        self::assertCount(1, $result);
        self::assertInstanceOf(Article::class, $result->get(0));
    }

    public function testEqualsForDateTime()
    {
        $criteria = new EqualsCriteria('date', '08.11.2016');
        $result = $this->collection->filter($criteria);
        self::assertCount(2, $result);
    }

    public function testEqualsForString()
    {
        $criteria = new EqualsCriteria('name', 'first');
        $result = $this->collection->filter($criteria);
        self::assertCount(1, $result);
    }

    public function testAndCriteria()
    {
        $firstCriteria = new EqualsCriteria('status', 1);
        $secondCriteria = new EqualsCriteria('name', 'first');
        $andCriteria = new AndCriteria($firstCriteria, $secondCriteria);
        $result = $this->collection->filter($andCriteria);
        self::assertCount(1, $result);
        self::assertEquals('first', $result->get(0)->getName());
        self::assertEquals(1, $result->get(0)->getStatus());
    }

    public function testOrCondition()
    {
        $firstCriteria = new EqualsCriteria('status', 1);
        $secondCriteria = new EqualsCriteria('name', 'second');
        $orCriteria = new OrCriteria($firstCriteria, $secondCriteria);
        $result = $this->collection->filter($orCriteria);
        self::assertCount(2, $result);
    }

    public function testLikeCriteria()
    {
        $criteria = new LikeCriteria('name', 's');
        $result = $this->collection->filter($criteria);
        self::assertCount(2, $result);
    }

    public function testCriteriaShouldThrowException()
    {
        $criteria = new LikeCriteria('status', 's');
        $this->expectException(CriteriaException::class);
        $this->collection->filter($criteria);
    }
}
