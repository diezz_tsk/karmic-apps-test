<?php

use App\Collection;
use App\Exceptions\ParserException;
use App\Models\Article;
use App\Services\Parsers\ArticleParser;

class ArticleParserTest extends PHPUnit_Framework_TestCase
{
    public function testParserShouldReturnCollection()
    {
        $result = (new ArticleParser())->parse([]);
        self::assertInstanceOf(Collection::class, $result);
    }

    public function testEachItemShouldBeAnArticle()
    {
        $data = [
            [
                'first',
                '08.11.2016',
                1,
            ],
        ];
        $result = (new ArticleParser())->parse($data);
        /* @var Article $first */
        $first = $result->get('first');
        self::assertInstanceOf(Article::class, $first);
        self::assertEquals('first', $first->getName());
        self::assertInstanceOf(\DateTime::class, $first->getDate());
        self::assertEquals(8, $first->getDate()->format('d'));
        self::assertEquals(11, $first->getDate()->format('m'));
        self::assertEquals(2016, $first->getDate()->format('Y'));
        self::assertEquals(1, $first->getStatus());
    }

    public function testParserShouldThrowException()
    {
        $data = [
            [
                'first',
                'this is a wrong date',
                1,
            ],
        ];
        $this->expectException(ParserException::class);
        (new ArticleParser())->parse($data);
    }
}
