<?php

use App\Collection;
use App\Exceptions\ItemNotFoundException;
use App\Model;

class FakeModel extends Model {
    public $field;

    /**
     * FakeModel constructor.
     *
     * @param $field
     */
    public function __construct($field)
    {
        $this->field = $field;
    }

}

class CollectionTest extends PHPUnit_Framework_TestCase
{
    public function testCreationCollection()
    {
        $collection = Collection::make();
        self::assertInstanceOf(Collection::class, $collection);
    }

    public function testPutByKeyMethod()
    {
        $collection = Collection::make();
        $one = new FakeModel(1);
        $two = new FakeModel(2);
        $collection->put('one', $one);
        $collection->put('two', $two);
        self::assertEquals($collection->get('one'), $one);
        self::assertEquals($collection->get('two'), $two);
    }

    public function testGetMethodShouldThrownException()
    {
        $collection = Collection::make();
        $this->expectException(ItemNotFoundException::class);
        $collection->get('throw');
    }

    public function testCountMethod()
    {
        $collection = Collection::make();
        self::assertEquals(0, $collection->count());
        $collection->put('one', new FakeModel(1));
        self::assertEquals(1, $collection->count());
        $collection->put('two', new FakeModel(1));
        self::assertEquals(2, $collection->count());
    }

    public function testFirstMethod()
    {
        $collection = Collection::make();
        self::assertNull($collection->first());
        $one = new FakeModel(1);
        $two = new FakeModel(2);
        $collection->put('one', $one);
        $collection->put('two', $two);
        self::assertEquals($one, $collection->first());
    }
}
