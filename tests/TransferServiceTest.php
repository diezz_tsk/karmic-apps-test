<?php

use App\Collection;
use App\Exceptions\FileNotFoundException;
use App\Models\Article;
use App\Service\ReaderService;
use App\Services\Parsers\ArticleParser;

class TransferServiceTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
    }

    public function testReadMethodShouldReturnACollection()
    {
        $csvFile = __DIR__ . '/data/test.csv';
        $parser = new ArticleParser();
        $service = new ReaderService($csvFile, $parser);
        $result = $service->read();
        self::assertInstanceOf(Collection::class, $result);
        self::assertCount(2, $result);
        foreach ($result as $article) {
            self::assertInstanceOf(Article::class, $article);
        }
    }

    public function testReadMethodShouldThrowException()
    {
        $csvFile = 'wrong_file_path.csv';
        $parser = new ArticleParser();
        $service = new ReaderService($csvFile, $parser);
        $this->expectException(FileNotFoundException::class);
        $service->read();
    }
}
