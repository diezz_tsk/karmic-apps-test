<?php

namespace App\Exceptions;

/**
 * Class UnknownPropertyException
 *
 * @package App\Exceptions
 */
class UnknownPropertyException extends \Exception
{

    /**
     * UnknownPropertyException constructor.
     *
     * @param string $fieldName имя вызываемого свойства.
     * @param string $className имя класса.
     */
    public function __construct($fieldName, $className)
    {
        $message = 'Getting unknown field: ' . $className . '::' . $fieldName;
        parent::__construct($message);
    }
}
