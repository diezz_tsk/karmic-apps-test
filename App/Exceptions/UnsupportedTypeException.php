<?php

namespace App\Exceptions;

/**
 * Class UnsupportedTypeException
 *
 * @package App\Exceptions
 */
class UnsupportedTypeException extends \Exception
{
    /**
     * UnsupportedTypeException constructor.
     *
     * @param string $type
     */
    public function __construct($type)
    {
        $message = 'Type :' . $type . 'don\'t support by System';
        parent::__construct($message);
    }
}
