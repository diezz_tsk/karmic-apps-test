<?php

namespace App\Exceptions;

/**
 * Class TypeConversionException
 *
 * @package App\Exceptions
 */
class TypeConversionException extends \Exception
{

}