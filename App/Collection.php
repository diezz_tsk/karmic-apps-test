<?php

namespace App;

use App\Contracts\FilterCriteriaInterface;
use App\Exceptions\ItemNotFoundException;
use Traversable;

/**
 * Class Collection
 * Класс-обертка над массивом.
 *
 * @package App
 */
class Collection implements \IteratorAggregate, \Countable
{
    /**
     * Массив элеменитов коллекции.
     *
     * @var Model[]
     */
    protected $items = [];

    /**
     * Collection constructor.
     *
     * @param array $items массив элементов коллекции.
     */
    public function __construct(array $items = [])
    {
        foreach ($items as $key => $item) {
            $this->put($key, $item);
        }
    }

    /**
     * Добавляет элемент в конец коллекции.
     *
     * @param Model $item добавляемый элемент.
     *
     * @return $this
     */
    public function push(Model $item)
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     * Добавляеи элемент в коллекцию по заданному ключу.
     *
     * @param mixed $key  ключ элемента коллекции.
     * @param Model $item добавляемый элемент.
     *
     * @return $this
     */
    public function put($key, Model $item)
    {
        $this->items[$key] = $item;
        return $this;
    }

    /**
     * Возвращаети элемент коллекции по заданному ключу.
     *
     * @param mixed $key ключ, по которому требуется вернуть элемент.
     *
     * @return Model
     * @throws ItemNotFoundException в случае, если элемент по заданному ключу не найден в
     *                               коллекции.
     */
    public function get($key)
    {
        if (!array_key_exists($key, $this->items)) {
            throw new ItemNotFoundException('Item with key: ' . $key . 'does\'t exist in collection');
        }

        return $this->items[$key];
    }

    /**
     * Возвращает все элементы коллекции.
     *
     * @return Model[]
     */
    public function all()
    {
        return $this->items;
    }

    /**
     * Производит фильтрацию элементов коллекции по заданному условию.
     *
     * @param mixed $criteria условие фильтрации.
     *
     * @return Collection
     */
    public function filter(FilterCriteriaInterface $criteria)
    {
        return $criteria->filter($this);
    }

    /**
     * Возвращает экземпляр коллекции.
     *
     * @param array $items массив элементов коллекции.
     *
     * @return static
     */
    public static function make(array $items = [])
    {
        return new static($items);
    }

    /**
     * Возвращает количество элементов в коллекции.
     *
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * Retrieve an external iterator.
     *
     * @return Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    /**
     * Мержит коллекции и возвращает новую.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    public function merge(Collection $collection)
    {
        return static::make(array_merge($this->all(), $collection->all()));
    }

    /**
     * Возвращает первый элемент коллекции.
     *
     * @return Model
     */
    public function first()
    {
        if (0 === count($this->items)) {
            return null;
        }

        $values = array_values($this->items);
        return $values[0];
    }
}
