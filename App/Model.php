<?php

namespace App;

use App\Exceptions\UnknownPropertyException;
use App\TypeHinting\BaseType;

/**
 * Class Model
 * Базовый класс модели.
 *
 * @package App
 */
class Model
{
    /**
     * Метод для получения значения приватного или защищенного свойства класса.
     * Возвращает значения геттера, если он задан для свойства.
     *
     * @param string $name имя получаемого свойства класса.
     *
     * @return mixed
     * @throws UnknownPropertyException в случае, если не установлен геттер либо свойство не
     *                                  опредеоенно в классе.
     */
    public function __get($name)
    {
        $getter = 'get' . ucfirst($name);
        if (!method_exists($this, $getter)) {
            throw new UnknownPropertyException($name, static::class);
        }

        return $this->$getter();
    }

    /**
     * @param $fieldName
     */
    public function getType($fieldName)
    {
        $class = new \ReflectionClass($this);
        try {
            $field = $class->getProperty($fieldName);
        } catch (\ReflectionException $e) {
            throw new UnknownPropertyException($fieldName, static::class);
        }
        $comment = $field->getDocComment();
        $type = '';
        if (preg_match('/@var\W+(\w+)/i', $comment, $match)) {
            $type = strtolower($match[1]);
        }

        switch ($type) {
            case strpos('int', $type):
                return BaseType::INT;
            case strpos('datetime', $type):
                return BaseType::DATETIME;
            default:
            case strpos('str', $type):
                return BaseType::STRING;
        }
    }
}
