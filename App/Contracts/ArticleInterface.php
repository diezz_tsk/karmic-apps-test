<?php

namespace App\Contracts;

/**
 * Interface ArticleInterface
 * Интерфейс, описывающий модель статьи.
 *
 * @package App\Contracts
 */
interface ArticleInterface
{
    /**
     * Возвращает имя статьи.
     *
     * @return string
     */
    public function getName();

    /**
     * Возвращает дату статьи.
     *
     * @return \DateTime
     */
    public function getDate();

    /**
     * Возвращает статус статьи.
     *
     * @return int
     */
    public function getStatus();
}
