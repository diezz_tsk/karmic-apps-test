<?php

namespace App\Contracts;

use App\Collection;

interface FilterCriteriaInterface
{
    /**
     * Фильтрует элементы коллекции по заданному правилу.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    public function filter(Collection $collection);
}