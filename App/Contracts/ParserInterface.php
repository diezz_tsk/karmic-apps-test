<?php

namespace App\Contracts;

use App\Collection;

/**
 * Interface ParserInterface
 *
 * @package App\Contracts
 */
interface ParserInterface
{
    /**
     * Парсит данные.
     *
     * @param array $data массив данных для парсера.
     *
     * @return Collection
     */
    public function parse(array $data);
}