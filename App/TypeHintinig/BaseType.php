<?php

namespace App\TypeHinting;

use App\Exceptions\UnsupportedTypeException;

/**
 * Class BaseType
 * Базовый класс типа переменной.
 *
 * @package App\TypeHinting
 */
abstract class BaseType
{
    const INT = 'Int';
    const STRING = 'String';
    const DATETIME = 'DateTime';

    /**
     * Производит по возможности приведение типа к заданному.
     *
     * @param mixed $var переменная, значение которой необходимо привести к требуемому типу.
     *
     * @return mixed
     */
    abstract public function convert($var);

    /**
     * Возвращает соотвествущий инстанс в зависимости от переданного типа.
     *
     * @param int $type тип переменной.
     *
     * @return BaseType
     * @throws UnsupportedTypeException в случае неподдерживаемого типа.
     */
    public static function getInstance($type)
    {
        switch ($type) {
            case self::DATETIME:
                return new DateTimeType();
            case self::STRING:
                return new StringType();
            case self::INT:
                return new IntegerType();
            default:
                throw new UnsupportedTypeException($type);
        }
    }

    /**
     * BaseType constructor.
     */
    protected function __construct()
    {
    }
}
