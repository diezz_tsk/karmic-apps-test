<?php

namespace App\TypeHinting;

use App\Exceptions\TypeConversionException;

/**
 * Class DateTimeType
 *
 * @package App\TypeHinting
 */
class DateTimeType extends BaseType
{
    /**
     * Производит по возможности приведение типа к заданному.
     *
     * @param mixed $var переменная, значение которой необходимо привести к требуемому типу.
     *
     * @throws TypeConversionException в случае невозможности преобразовать в DateTime.
     * @return mixed
     */
    public function convert($var)
    {
        if ($var instanceof \DateTime) {
            return $var;
        }

        try {
            $date = new \DateTime($var);
        } catch (\Exception $e) {
            throw new TypeConversionException('Unable to convert var to DateTime');
        }

        return $date;
    }
}
