<?php

namespace App\TypeHinting;

/**
 * Class StringType
 *
 * @package App\TypeHinting
 */
class StringType extends BaseType
{
    /**
     * Производит по возможности приведение типа к заданному.
     *
     * @param mixed $var переменная, значение которой необходимо привести к требуемому типу.
     *
     * @return mixed
     */
    public function convert($var)
    {
        return (string)$var;
    }
}
