<?php

namespace App\TypeHinting;

/**
 * Class IntegerType
 *
 * @package App\TypeHinting
 */
class IntegerType extends BaseType
{
    /**
     * Производит по возможности приведение типа к заданному.
     *
     * @param mixed $var переменная, значение которой необходимо привести к требуемому типу.
     *
     * @return mixed
     */
    public function convert($var)
    {
        return (int)$var;
    }
}
