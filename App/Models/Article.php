<?php

namespace App\Models;

use App\Contracts\ArticleInterface;
use App\Model;

/**
 * Class Article
 * Модель, описыающая статью.
 *
 * @package App\Models
 */
class Article extends Model implements ArticleInterface
{
    /**
     * Название статьи.
     *
     * @var string
     */
    protected $name;
    /**
     * Дата создания статьи.
     *
     * @var \DateTime
     */
    protected $date;
    /**
     * Статус статьи.
     *
     * @var int
     */
    protected $status;

    /**
     * Article constructor.
     *
     * @param string    $name
     * @param \DateTime $date
     * @param int       $status
     */
    public function __construct($name, \DateTime $date, $status)
    {
        $this->name = $name;
        $this->date = $date;
        $this->status = $status;
    }

    /**
     * Возвращает имя статьи.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Возвращает дату статьи.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Возвращает статус статьи.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}
