<?php

namespace App\Service;

use App\Collection;
use App\Contracts\ParserInterface;
use App\Exceptions\FileNotFoundException;
use App\Exceptions\TransferServiceException;

/**
 * Class TransferService
 *
 * @package App\Service
 */
class ReaderService
{
    /**
     * Разделитель значений в csv файле.
     */
    const CSV_DELIMITER = ';';
    /**
     * Флаг для режима чтения файла.
     */
    const FILE_OPEN_MODE_READ = 'r';
    /**
     * Флаг для режима записи в файл.
     */
    const FILE_OPEN_MODE_WRITE = 'w';
    /**
     * Путь до csv файла.
     *
     * @var string
     */
    private $csvFile;
    /**
     * Парсер для обработки csv файла.
     *
     * @var ParserInterface
     */
    private $parser;

    /**
     * TransferService constructor.
     *
     * @param string          $csvFile
     * @param ParserInterface $parser
     */
    public function __construct($csvFile, ParserInterface $parser)
    {
        $this->csvFile = $csvFile;
        $this->parser = $parser;
    }

    /**
     * Читает и парсит csv файл.
     *
     * @return Collection
     */
    public function read()
    {
        $data = $this->getData();
        return $this->parser->parse($data);
    }

    /**
     * Читает данные из csv файла.
     *
     * @return array
     * @throws FileNotFoundException в случае, если файл не найден.
     */
    private function getData()
    {
        $handle = $this->getFileHandle(self::FILE_OPEN_MODE_READ);
        $data = [];
        while (($row = fgetcsv($handle, 1000, self::CSV_DELIMITER)) !== false) {
            $data[] = $row;
        }
        fclose($handle);
        return $data;
    }

    /**
     * Возвращает указаетель на открытый файл.
     *
     * @param string $mode режим открытия файла.
     *
     * @return resource
     * @throws FileNotFoundException в случае, если файл не найден.
     * @throws TransferServiceException в счлуе, если невозможно открыть файл.
     */
    private function getFileHandle($mode)
    {
        if (!file_exists($this->csvFile)) {
            throw new FileNotFoundException('Csv file: ' . $this->csvFile . ' was\'t found');
        }

        $handle = fopen($this->csvFile, $mode);
        if (false === $handle) {
            throw new TransferServiceException('Can\'t open file.');
        }

        return $handle;
    }
}
