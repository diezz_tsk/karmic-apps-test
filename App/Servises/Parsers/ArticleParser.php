<?php

namespace App\Services\Parsers;

use App\Collection;
use App\Contracts\ArticleInterface;
use App\Contracts\ParserInterface;
use App\Exceptions\ParserException;
use App\Models\Article;

/**
 * Class ArticleParser
 *
 * @package App\Services\Parsers
 */
class ArticleParser implements ParserInterface
{
    /**
     * Ключ по которому хранится имя статьи.
     */
    const ARTICLE_NAME_OFFSET = 0;
    /**
     * Ключ по которому хранится дата статьи.
     */
    const ARTICLE_DATE_OFFSET = 1;
    /**
     * Ключ по каоторому хранится статус статьи.
     */
    const ARTICLE_STATUS_OFFSET = 2;

    /**
     * Парсит данные и возвращает коллекцию статей.
     *
     * @param array $data массив строк csv файла.
     *
     * @throws ParserException в случае неверного формата даты в одной из строк csv файла.
     * @return Collection
     */
    public function parse(array $data)
    {
        $collection = Collection::make();
        foreach ($data as $item) {
            $article = $this->getArticle($item);
            $collection->put($article->getName(), $article);
        }

        return $collection;
    }

    /**
     * Создает и возвращает экземпляр класса статьи.
     *
     * @param array $data строка csv файла.
     *
     * @throws ParserException в случае неверного формата даты в csv файле.
     * @return ArticleInterface
     */
    private function getArticle(array $data)
    {
        $name = $this->getArticleName($data);
        $date = $this->getArticleDate($data);
        $status = $this->getArticleStatus($data);
        return new Article($name, $date, $status);
    }

    /**
     * Возвращает имя статьи.
     *
     * @param array $data строка csv файла.
     *
     * @return string
     */
    private function getArticleName(array $data)
    {
        return (string)$this->getValue($data, self::ARTICLE_NAME_OFFSET, '');
    }

    /**
     * Возвращает дату статьи.
     *
     * @param array $data строка csv файла.
     *
     * @throws ParserException в случае неверного формата даты в csv файле.
     * @return \DateTime
     */
    private function getArticleDate(array $data)
    {
        $default = new \DateTime();
        $date = $this->getValue($data, self::ARTICLE_DATE_OFFSET, $default);
        if ($date instanceof \DateTime) {
            return $date;
        }

        try {
            $date = new \DateTime($date);
        } catch (\Exception $e) {
            throw new ParserException('Format of the Article Date must be compatible with 
            format specified in manual http://php.net/manual/en/datetime.construct.php');
        }

        return $date;
    }

    /**
     * Возвращает статус статьи.
     *
     * @param array $data строка csv файла.
     *
     * @return int
     */
    private function getArticleStatus(array $data)
    {
        return (int)$this->getValue($data, self::ARTICLE_STATUS_OFFSET, 1);
    }

    /**
     * Возвращает значение по заданному ключу. В случае если ключа не существует, возвращает
     * значение по умолчанию.
     *
     * @param array      $data    массив, в котором производится поиск.
     * @param string|int $key     ключ, по которому необходимо вернуть значение.
     * @param mixed      $default значение по умолчанию, которое будет возвращено в случае, если
     *                            ключа не существует.
     *
     * @return mixed
     */
    private function getValue(array $data, $key, $default)
    {
        return array_key_exists($key, $data) ? $data[$key] : $default;
    }
}
