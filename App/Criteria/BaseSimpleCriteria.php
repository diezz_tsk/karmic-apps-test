<?php

namespace App\Criteria;

use App\Collection;
use App\Contracts\FilterCriteriaInterface;
use App\Exceptions\CriteriaException;
use App\Model;
use App\TypeHinting\BaseType;

/**
 * Class BaseSimpleCriteria
 * Базоый класс для простой критерии.
 *
 * @package App\Criteria
 */
abstract class BaseSimpleCriteria implements FilterCriteriaInterface
{
    /**
     * Имя поля, для которкого выполнятся фильтрация.
     *
     * @var string
     */
    protected $field;
    /**
     * Значение, с которым происходит сравнение.
     *
     * @var mixed
     */
    protected $value;

    /**
     * BaseCriteria constructor.
     *
     * @param $field
     * @param $value
     */
    public function __construct($field, $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * Фильтрует элементы коллекции по заданному правилу.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    public function filter(Collection $collection)
    {
        $this->checkType($collection->first());
        $result = Collection::make();
        foreach ($collection as $model) {
            /* @var Model $model */
            $value = BaseType::getInstance($model->getType($this->field))->convert($this->value);
            $compareResult = $this->compare($model->{$this->field}, $value);
            if (true === $compareResult) {
                $result->push($model);
            }
        }

        return $result;
    }

    /**
     * Проверяет соотвествие типа поля модели, доступному типу критерии.
     *
     * @param Model $model модель, поле которой проверяется.
     *
     * @throws CriteriaException в случае, если тип поля не поддерживатеся данной критерий.
     * @return void
     */
    protected function checkType(Model $model)
    {
        $type = $model->getType($this->field);
        $allowedTypes = $this->getAllowedTypes();
        if (false !== in_array('*', $allowedTypes, true)) {
            return;
        }

        if (!in_array($type, $allowedTypes, true)) {
            throw new CriteriaException('This criteria does\'t support type: ' . $type);
        }
    }

    /**
     * Возвращает массив доступных типов для данной критерии.
     * В случае, если для критерии доступны все типы, нужно вернуть ['*'];
     *
     * @return array
     */
    protected function getAllowedTypes()
    {
        return ['*'];
    }

    /**
     * Произвоит сравнение двух значений.
     *
     * @param mixed $left  левый операнд.
     * @param mixed $right правый операнд.
     *
     * @return boolean
     */
    abstract protected function compare($left, $right);
}