<?php

namespace App\Criteria;

use App\TypeHinting\BaseType;

/**
 * Class LikeCriteria
 * Ищет подстроку в текстовом поле.
 *
 * @package App\Criteria
 */
class LikeCriteria extends BaseSimpleCriteria
{
    /**
     * Произвоит сравнение двух значений.
     *
     * @param mixed $left  левый операнд.
     * @param mixed $right правый операнд.
     *
     * @return boolean
     */
    protected function compare($left, $right)
    {
        return false !== strpos($left, $right);
    }

    /**
     * Возвращает массив доступных типов для данной критерии.
     * В случае, если для критерии доступны все типы, нужно вернуть ['*'];
     *
     * @return array
     */
    protected function getAllowedTypes()
    {
        return [BaseType::STRING];
    }
}
