<?php

namespace App\Criteria;

use App\Contracts\FilterCriteriaInterface;

/**
 * Class BaseComplexCriteria
 * Базовый класс для состановной критерии.
 *
 * @package App\Criteria
 */
abstract class BaseComplexCriteria implements FilterCriteriaInterface
{
    /**
     * Левый операнд.
     *
     * @var FilterCriteriaInterface
     */
    protected $left;
    /**
     * Правый операнд.
     *
     * @var FilterCriteriaInterface
     */
    protected $right;

    /**
     * AndCriteria constructor.
     *
     * @param FilterCriteriaInterface $left
     * @param FilterCriteriaInterface $right
     */
    public function __construct(FilterCriteriaInterface $left, FilterCriteriaInterface $right)
    {
        $this->left = $left;
        $this->right = $right;
    }
}
