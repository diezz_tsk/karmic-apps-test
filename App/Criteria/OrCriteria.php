<?php

namespace App\Criteria;

use App\Collection;

/**
 * Class OrCriteria
 * Составная критерия - логиеское ИЛИ.
 *
 * @package App\Criteria
 */
class OrCriteria extends BaseComplexCriteria
{
    /**
     * Фильтрует элементы коллекции по заданному правилу.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    public function filter(Collection $collection)
    {
        return $this->left->filter($collection)->merge($this->right->filter($collection));
    }
}
