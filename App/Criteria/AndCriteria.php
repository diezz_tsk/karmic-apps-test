<?php

namespace App\Criteria;

use App\Collection;

/**
 * Class AndCriteria
 * Составная критерия - логическое И.
 *
 * @package App\Criteria
 */
class AndCriteria extends BaseComplexCriteria
{
    /**
     * Фильтрует элементы коллекции по заданному правилу.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    public function filter(Collection $collection)
    {
        $leftResult = $this->left->filter($collection);
        return $this->right->filter($leftResult);
    }
}
