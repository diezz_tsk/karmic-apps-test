<?php

namespace App\Criteria;

/**
 * Class EqualsCriteria
 * Простая критерия, проверяет равенство двух операндов.
 *
 * @package App\Criteria
 */
class EqualsCriteria extends BaseSimpleCriteria
{
    /**
     * Произвоит сравнение двух значений.
     *
     * @param mixed $left  левый операнд.
     * @param mixed $right правый операнд.
     *
     * @return boolean
     */
    protected function compare($left, $right)
    {
        return $left == $right;
    }
}
